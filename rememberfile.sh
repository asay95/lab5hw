#!/bin/bash
## 22 Reminder Utility
## Write a script that remembers what the user wrote
## then add what they wrote to a different file
## that is used for reminding them what they wrote.

echo "Hello, what would you like to add to your list of reminders?"
read -p "What would you like to remember? " remember

echo "$remember" >> reminder.sh
echo "To bring up your list of reminders, please enter the command: "cat reminder.sh"."
