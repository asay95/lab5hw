#!/bin/bash
## 26 Keeping Track of Events
## Make an agenda that keeps track of events and their dates

## Prompt for user input on event and date
echo "Hello, what would you like to add to your agenda?"
read -p "Please enter a date (mm/dd/yyyy): " date
read -p "Please enter the event you will have on this date: " event
todate=$(date +"%x")
dayoftheweek=$(date +"%a")
dayofthemonth=$(date +"%d")

## Store user input into agenda file
echo "($date: $event)" >> agendafile.sh

## Let the user know how to bring up the agenda file
echo "Event '$event' added to agenda for the date of $date."
echo "To bring up your agenda, please enter the command: "cat agendafile.sh"."

